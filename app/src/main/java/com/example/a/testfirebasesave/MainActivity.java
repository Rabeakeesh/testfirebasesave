package com.example.a.testfirebasesave;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends Activity {
    //android layout
    private EditText titel, subject, text, date, stars, status;
    private Button save;

    SaveData saveData;
    DatabaseReference db;
    FirebaseHelper helper;
    CustomAdapter adapter;
    ListView lv;


    //Firebase var
    private DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        lv = (ListView) findViewById(R.id.listview);

        //INITIALIZE FIREBASE DB
        db = FirebaseDatabase.getInstance().getReference();
        helper = new FirebaseHelper(db);

        //ADAPTER
        adapter = new CustomAdapter(this, helper.retrieve());
        lv.setAdapter(adapter);



        databaseReference = FirebaseDatabase.getInstance().getReference().child("Tasks");

        titel = (EditText) findViewById(R.id.titel);
        subject = (EditText) findViewById(R.id.subject);
        text = (EditText) findViewById(R.id.text);
        date = (EditText) findViewById(R.id.date);
        stars = (EditText) findViewById(R.id.stars);
        status = (EditText) findViewById(R.id.status);
        saveData = new SaveData();

        save = (Button) findViewById(R.id.save);


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                databaseReference.push().child(titel.getText().toString());
//                AddDate();
//                Intent save = new Intent(MainActivity.this, Secound.class);
//                startActivity(save);
//            }
//        });

                //GET DATA
                String Titel = titel.getText().toString();
                String Text = text.getText().toString();
                String Date = date.getText().toString();

                //SET DATA
                SaveData s = new SaveData();
                s.setTitel(Titel);
                s.setText(Text);
                s.setDate(Date);

                //SIMPLE VALIDATION
                if (titel != null && titel.length() > 0) {
                    //THEN SAVE
                    if (helper.save(s)) {
                        //IF SAVED CLEAR EDITXT
                        titel.setText("");
                        text.setText("");
                        date.setText("");

                        adapter = new CustomAdapter(MainActivity.this, helper.retrieve());
                        lv.setAdapter(adapter);

                    }
                } else {
                    Toast.makeText(MainActivity.this, "Name Must Not Be Empty", Toast.LENGTH_SHORT).show();
                }

            }
        });

     //   d.show();
    }


    private void setSupportActionBar(Toolbar toolbar) {
    }

    public void AddDate() {

        String Titel = titel.getText().toString().trim();
        String Subject = subject.getText().toString().trim();
        String Text = text.getText().toString().trim();
        String Date = date.getText().toString().trim();
        String Stars = stars.getText().toString().trim();
        String Status = status.getText().toString().trim();


        SaveData saveData = new SaveData(Titel, Subject, Text, Date, Stars, Status);

        //databaseReference.setValue(saveData);
        databaseReference.push().setValue(saveData);

    }
//    private void getValues(){
//
//        saveData.setTitel(titel.getText().toString());
//        saveData.setDate(date.getText().toString());
//        saveData.setText(text.getText().toString());
//
//    }
//    public void INSERT(){
//        databaseReference.child("User").child(saveData.getTitel()).setValue(saveData);
//
//        Intent intent = new Intent(MainActivity.this, Secound.class);
//        startActivity(intent);
//
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                getValues();
//                databaseReference.child("ID001").setValue(titel);
//                Toast.makeText(MainActivity.this, "Data Insert", Toast.LENGTH_LONG).show();
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }

}

