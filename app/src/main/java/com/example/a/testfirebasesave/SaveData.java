package com.example.a.testfirebasesave;

public class SaveData {

    String titel;
    String subject;
    String text;
    String date;
    String stars;
    String status;

    public SaveData(String titel, String subject, String text, String date, String stars, String status) {
        this.titel = titel;
        this.subject = subject;
        this.text = text;
        this.date = date;
        this.stars = stars;
        this.status = status;
    }

    public SaveData() {

    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStars() {
        return stars;
    }

    public void setStars(String stars) {
        this.stars = stars;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void add(SaveData saveData) {
    }
}
