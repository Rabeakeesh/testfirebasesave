package com.example.a.testfirebasesave;

public class User {

    public String Name;
    public String Email;

    public User(String name, String email, String ID) {
        Name = name;
        Email = email;
        this.ID = ID;
    }

    public User(String name, String email) {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String ID;
}
